package permit

/*******************************************************************************
Copyright:cloud
Author:cloudapex@126.com
Version:1.0
Date:2020-07-21
Description: 公共数据
*******************************************************************************/
import (
	"sync"

	"gitee.com/cloudapex/permit/cache"
	"gitee.com/cloudapex/ulern/rds"
)

const (
	// 数据权限范围数据存储字段
	DEF_PERMIT_HEAD_FIELD = "DataPermitScope"
)

var (
	initRdb sync.Once
)

func UseRdb(p *rds.Pool) { initRdb.Do(func() { cache.TheRdb = p }) }
