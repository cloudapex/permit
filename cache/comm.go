package cache

/*******************************************************************************
Copyright:cloud
Author:cloudapex@126.com
Version:1.0
Date:2020-07-21
Description: 公共结构
*******************************************************************************/

type UserDevce struct {
	OsTyp string `json:"osTyp"`
	Devce string `json:"devce"`
}
type RoleScope struct {
	RId   int64  `json:"rId"`
	Scope string `json:"scope"`
}
