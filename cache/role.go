package cache

/*******************************************************************************
Copyright:cloud
Author:cloudapex@126.com
Version:1.0
Date:2020-07-21
Description: 用户角色模型
*******************************************************************************/
import (
	"encoding/json"
	"fmt"

	"gitee.com/cloudapex/ulern/rds"
	"gitee.com/cloudapex/ulern/util"
)

type SUserRole struct {
	Id   int64       `json:"-"`    // 用户Id
	Upd  int         `json:"upd"`  // 是否有更新
	RIds []int64     `json:"rids"` // 此用户拥有的(角色Ids)
	Devs []UserDevce `json:"devs"` // 此用户登陆设备信息[Windows Android MacOS IOS Linux]
}

func (this *SUserRole) Key() *rds.String {
	return &rds.String{Key: rds.Key{
		"MD_Permit", fmt.Sprintf("PERMIT_USER_ROLES_%d", this.Id), "json"}}
}

func (this *SUserRole) Sec() int64 { return 5 * 24 * 3600 }

func (this *SUserRole) Load() error {
	return this.Key().Get().Unmarshal(this)
}
func (this *SUserRole) Store(isLogin bool) error {
	util.Cast(isLogin, func() { this.Upd = 0 }, func() { this.Upd = 1 })
	util.Cast(len(this.RIds) == 0, func() { this.RIds = []int64{} }, nil)
	util.Cast(len(this.Devs) == 0, func() { this.Devs = []UserDevce{} }, nil)
	buf, _ := json.Marshal(this)
	if isLogin {
		return this.Key().SetEX(buf, this.Sec()).Error()
	}
	return this.Key().Set(buf).Error()
}
func (this *SUserRole) Remove() error {
	return this.Key().Del().Error()
}

// ------------ logic

// SUserRole.UpdTDev 更新登陆设备类型
func (this *SUserRole) UpdTDev(osName string, devId string) {
	exist := false
	for n, it := range this.Devs {
		if it.OsTyp == osName {
			exist = true
			this.Devs[n].Devce = devId
		}
	}
	if !exist {
		this.Devs = append(this.Devs, UserDevce{osName, devId})
	}
}

// SUserRole.Matched 设备类型是否匹配
func (this *SUserRole) Matched(osName string, devId string) bool {
	if len(this.Devs) == 0 {
		return true // 宽松约束
	}
	for _, it := range this.Devs {
		if it.OsTyp == osName && it.Devce != devId {
			return false
		}
	}
	return true
}
