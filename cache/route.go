package cache

/*******************************************************************************
Copyright:cloud
Author:cloudapex@126.com
Version:1.0
Date:2020-07-21
Description: 路由角色模型
*******************************************************************************/
import (
	"encoding/json"
	"fmt"

	"gitee.com/cloudapex/ulern/rds"
)

type HRouteRole struct {
	CoId   int64       `json:"-"`      // 公司Id
	Route  string      `json:"-"`      // 路由名称
	RIds   []int64     `json:"rIds"`   // 此路由关联的角色Ids
	Scopes []RoleScope `json:"scopes"` // 此路由关联的(不同角色对应的数据权限)
}

func (this *HRouteRole) Key() *rds.Hash {
	return &rds.Hash{Key: rds.Key{
		"MD_Permit", fmt.Sprintf("PERMIT_ROUTE_ROLES_%d", this.CoId), ""}}
}

func (this *HRouteRole) Load() error {
	return this.Key().HGet(this.Route).Unmarshal(this)
}
func (this *HRouteRole) Store() error {
	buf, _ := json.Marshal(this)
	return this.Key().HSet(this.Route, buf).Error()
}
func (this *HRouteRole) Remove() error {
	return this.Key().HDel(this.Route).Error()
}
func (this *HRouteRole) Clear() error {
	return this.Key().Del().Error()
}
